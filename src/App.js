/*eslint-disable*/
import React, { Component } from "react";
import "./App.css";
import Wrapper from "./HOC/Wrapper";
import Login from "./Components/Login";
import Playground from "./Components/Playground";

class App extends Component {
  constructor() {
    super();
    this.state = {
      playernames: []
    };
  }
  playernames = names => {
    this.setState({ playernames: names });
  };

  resetPlayers = () => {
    console.log("Called");
    this.setState({ playernames: [] });
  };

  render() {
    return (
      <Wrapper>
        {!this.state.playernames.length ? (
          <Login playernames={this.playernames} />
        ) : (
          <Playground
            players={this.state.playernames}
            resetPlayers={this.resetPlayers}
          />
        )}
      </Wrapper>
    );
  }
}
export default App;

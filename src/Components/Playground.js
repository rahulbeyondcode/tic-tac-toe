/*eslint-disable */
import React, { Component } from "react";
import Wrapper from "../HOC/Wrapper";

import "../Style/style.css";

class Playground extends Component {
  constructor() {
    super();
    this.state = {
      playground: [
        { id: 1, value: "" },
        { id: 2, value: "" },
        { id: 3, value: "" },
        { id: 4, value: "" },
        { id: 5, value: "" },
        { id: 6, value: "" },
        { id: 7, value: "" },
        { id: 8, value: "" },
        { id: 9, value: "" }
      ],
      currentPlayerNotation: 0,
      players: [],
      winner: "",
      playedFields: []
    };
  }
  styles = {
    style1: {
      borderRight: "2px solid black",
      borderBottom: "2px solid black"
    },
    style2: {
      borderBottom: "2px solid black"
    },
    style3: {
      borderRight: "2px solid black"
    },
    styleBasic: {
      border: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      width: 80,
      height: 80,
      fontSize: 32,
      backgroundColor: "#FFFFB4",
      margin: 0
    },
    Wrapper: {
      display: "flex",
      flexWrap: "wrap",
      width: 150,
      position: "absolute",
      top: "40%",
      left: "30%"
    }
  };

  componentDidMount() {
    this.setState(
      { players: this.props.players },
      () =>
        this.state.players[0] === "Computer" &&
        this.computerMove(Math.floor(Math.random() * (10 - 1) + 1))
    );
  }

  // Invoked only when the match is with computer
  computerMove = move => {
    let playedFields = this.state.playedFields;
    const temporaryHolder = this.state.playground;

    if (
      this.state.playedFields.length !== 9 &&
      this.state.playedFields.includes(move)
    )
      this.computerMove(Math.floor(Math.random() * (10 - 1) + 1));
    else {
      temporaryHolder.map((play, index) => {
        if (play.id === move) {
          temporaryHolder[index].value = this.state.currentPlayerNotation
            ? "0"
            : "X";
          playedFields.push(move);
          this.setState({
            currentPlayerNotation: !this.state.currentPlayerNotation,
            playedFields
          });
        }
      });
      this.setState(
        {
          playground: temporaryHolder
        },
        () => {
          this.checkForWinner();
        }
      );
    }
  };

  // User moves are calculated here
  playerMove = id => {
    const temporaryHolder = this.state.playground;
    let playedFields = this.state.playedFields;
    temporaryHolder.map((play, index) => {
      if (play.id === id && play.value === "") {
        temporaryHolder[index].value = this.state.currentPlayerNotation
          ? "0"
          : "X";
        playedFields.push(index + 1);
        this.setState({
          currentPlayerNotation: !this.state.currentPlayerNotation,
          playedFields
        });
      }
    });
    this.setState(
      {
        playground: temporaryHolder
      },
      () => {
        (this.state.players[0] === "Computer" ||
          this.state.players[1] === "Computer") &&
          this.computerMove(Math.floor(Math.random() * (10 - 1) + 1));
        this.checkForWinner();
      }
    );
  };

  // Check if any one of the two players won the match
  checkForWinner = () => {
    let player1moves = [];
    let player2moves = [];
    let winCondition1 = [1, 2, 3];
    let winCondition2 = [4, 5, 6];
    let winCondition3 = [7, 8, 9];
    let winCondition4 = [1, 4, 7];
    let winCondition5 = [2, 5, 8];
    let winCondition6 = [3, 6, 9];
    let winCondition7 = [1, 5, 9];
    let winCondition8 = [3, 5, 7];

    this.state.playground.map((play, index) => {
      // Possible Winning situations = [123 or 456 or 789], [147 or 258 or 369], [159 or 357]
      if (play.value === "X") player1moves.push(index + 1);
      else if (play.value === "0") player2moves.push(index + 1);
    });

    if (
      winCondition1.every(element => player1moves.indexOf(element) > -1) ||
      winCondition2.every(element => player1moves.indexOf(element) > -1) ||
      winCondition3.every(element => player1moves.indexOf(element) > -1) ||
      winCondition4.every(element => player1moves.indexOf(element) > -1) ||
      winCondition5.every(element => player1moves.indexOf(element) > -1) ||
      winCondition6.every(element => player1moves.indexOf(element) > -1) ||
      winCondition7.every(element => player1moves.indexOf(element) > -1) ||
      winCondition8.every(element => player1moves.indexOf(element) > -1)
    )
      this.setState({ winner: this.state.players[0] });

    if (
      winCondition1.every(element => player2moves.indexOf(element) > -1) ||
      winCondition2.every(element => player2moves.indexOf(element) > -1) ||
      winCondition3.every(element => player2moves.indexOf(element) > -1) ||
      winCondition4.every(element => player2moves.indexOf(element) > -1) ||
      winCondition5.every(element => player2moves.indexOf(element) > -1) ||
      winCondition6.every(element => player2moves.indexOf(element) > -1) ||
      winCondition7.every(element => player2moves.indexOf(element) > -1) ||
      winCondition8.every(element => player2moves.indexOf(element) > -1)
    )
      this.setState({ winner: this.state.players[1] });
  };

  // Resetting for a new game
  resetAll = () => {
    this.setState(
      {
        playground: [
          { id: 1, value: "" },
          { id: 2, value: "" },
          { id: 3, value: "" },
          { id: 4, value: "" },
          { id: 5, value: "" },
          { id: 6, value: "" },
          { id: 7, value: "" },
          { id: 8, value: "" },
          { id: 9, value: "" }
        ],
        currentPlayerNotation: 0,
        winner: "",
        playedFields: []
      },
      () =>
        this.state.players[0] === "Computer" &&
        this.computerMove(Math.floor(Math.random() * (10 - 1) + 1))
    );
  };

  resetPlayers = () => {
    this.props.resetPlayers();
  };

  getClassName = index => {
    var finalClass = {};
    if (index === 0 || index === 1 || index === 3 || index === 4)
      Object.assign(finalClass, this.styles.styleBasic, this.styles.style1);
    else if (index === 2 || index === 5)
      Object.assign(finalClass, this.styles.styleBasic, this.styles.style2);
    else if (index === 6 || index === 7)
      Object.assign(finalClass, this.styles.styleBasic, this.styles.style3);
    else Object.assign(finalClass, this.styles.styleBasic);
    return finalClass;
  };

  render() {
    return (
      <Wrapper>
        <div className="mt-4">
          <h1 className="text-center text-success font-weight-bold">
            Tic Tac Toe
          </h1>
        </div>
        <div className="hero-section">
          <div className="playgroundOuter">
            <div className="playgroundWrapper">
              {this.state.playground.map((play, index) => (
                <div
                  key={play.id}
                  onClick={() => {
                    !this.state.winner.length ? this.playerMove(play.id) : null;
                  }}
                >
                  <p style={this.getClassName(index)}>
                    <b>{play.value}</b>
                  </p>
                </div>
              ))}
            </div>
          </div>
          <div className="playerInfo">
            <h4>
              Current Player :{" "}
              {!this.state.currentPlayerNotation
                ? this.state.players[0]
                : this.state.players[1]}{" "}
              (<span> {this.state.currentPlayerNotation ? "0" : "X"}</span> )
            </h4>
            {(this.state.winner || this.state.playedFields.length === 9) && (
              <Wrapper>
                <h4 className="winner">
                  Winner :{" "}
                  {this.state.winner ? `${this.state.winner}` : "No Winner"}
                </h4>
                <button onClick={() => this.resetAll()}>
                  Start A New Game
                </button>
                <button className="ml-3" onClick={() => this.resetPlayers()}>
                  Reset Players
                </button>
              </Wrapper>
            )}
          </div>
        </div>
      </Wrapper>
    );
  }
}
export default Playground;

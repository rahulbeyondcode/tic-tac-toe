/*eslint-disable */
import React, { Component } from "react";

import "../Style/style.css";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      player1: "",
      player2: "",
      pc1: false,
      pc2: false
    };
  }
  playerName = (name, player) => {
    switch (player) {
      case "f":
        this.setState({ player1: name });
        break;
      case "s":
        this.setState({ player2: name });
        break;
      case "c1":
        this.setState({
          player1: this.state.pc1 ? "" : "Computer",
          pc1: !this.state.pc1
        });
        break;
      case "c2":
        this.setState({
          player2: this.state.pc2 ? "" : "Computer",
          pc2: !this.state.pc2
        });
        break;
    }
  };
  render() {
    return (
      <div className="loginBg">
        <div className="loginScreen">
          <div className="loginwrapper">
            <p>
              Enter first player name {"(X)"}
              <input
                type="text"
                disabled={this.state.pc1}
                value={this.state.player1}
                onChange={event => this.playerName(event.target.value, "f")}
              />
            </p>

            <h6
              onClick={event =>
                !this.state.pc2 && this.playerName(event.target.value, "c1")
              }
            >
              <input
                type="checkbox"
                disabled={this.state.pc2}
                checked={this.state.pc1}
                onChange={event => this.playerName(event.target.value, "c1")}
              />{" "}
              Choose PC
            </h6>

            <p>
              Enter second player name {"(0)"}
              <input
                type="text"
                disabled={this.state.pc2}
                value={this.state.player2}
                onChange={event => this.playerName(event.target.value, "s")}
              />
            </p>

            <h6
              onClick={event =>
                !this.state.pc1 && this.playerName(event.target.value, "c2")
              }
            >
              <input
                type="checkbox"
                disabled={this.state.pc1}
                checked={this.state.pc2}
                onChange={event => this.playerName(event.target.value, "c2")}
              />{" "}
              Choose PC
            </h6>

            <button
              onClick={() =>
                this.state.player1.length && this.state.player2.length
                  ? this.props.playernames([
                      this.state.player1,
                      this.state.player2
                    ])
                  : null
              }
            >
              Start Game
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
